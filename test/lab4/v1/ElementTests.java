package lab4.v1;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;

public class ElementTests {
    private ByteArrayOutputStream stream;
    @Before
    public void setup()
    {
        stream = new ByteArrayOutputStream();
    }
    @Test
    public void SectionShouldReturnValidHtml() throws Exception {
        Section section = Section.builder()
                .withTitle("title")
                .addParagraph(new Paragraph("para"))
                .addParagraph(ParagraphWithList.builder()
                        .withText("text")
                        .withList(UnorderedList.builder()
                                .addItem(new ListItem("A"))
                                .build())
                        .build())
                .build();

        section.writeHTML(stream);
        String result = stream.toString();
        Assert.assertTrue(result.contains("<h2>"));
        Assert.assertTrue(result.contains("title"));
        Assert.assertTrue(result.contains("</h2>"));
        Assert.assertTrue(result.contains("<p>"));
        Assert.assertTrue(result.contains("para"));
        Assert.assertTrue(result.contains("</p>"));
        Assert.assertTrue(result.contains("text"));
        Assert.assertTrue(result.contains("<ul>"));
        Assert.assertTrue(result.contains("<li>"));
        Assert.assertTrue(result.contains("A"));
        Assert.assertTrue(result.contains("</li>"));
        Assert.assertTrue(result.contains("</ul>"));
    }
    @Test
    public void PictureShouldReturnValidHtml(){
        Picture picture = new Picture("some_url");

        picture.writeHTML(stream);
        String result = stream.toString();

        Assert.assertTrue(result.contains("<img"));
        Assert.assertTrue(result.contains("src"));
        Assert.assertTrue(result.contains("some_url"));
        Assert.assertTrue(result.contains("/>"));
    }
    @Test
    public void UnorderedListShouldReturnValidHtml() throws Exception {
        UnorderedList list = UnorderedList.builder()
                .addItem(new ListItem("A"))
                .addItem(new ListItem("b"))
                .addItem(new ListItem("c"))
                .build();
        list.writeHTML(stream);
        String result = stream.toString();

        Assert.assertTrue(result.contains("<ul>"));
        Assert.assertTrue(result.contains("<li>"));
        Assert.assertTrue(result.contains("A"));
        Assert.assertTrue(result.contains("b"));
        Assert.assertTrue(result.contains("c"));
        Assert.assertTrue(result.contains("</li>"));
        Assert.assertTrue(result.contains("</ul>"));
    }
}
