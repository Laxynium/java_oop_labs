package lab2;

//import com.sun.javaws.exceptions.InvalidArgumentException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;

public class MatrixTests {

    private Matrix matrix;

    @Before
    public void setUp()
    {
        //to remove
        matrix = new Matrix(2,3);
        matrix.set(0,0,0);
        matrix.set(0,1,1);
        matrix.set(0,2,2);
        matrix.set(1,0,3);
        matrix.set(1,1,4);
        matrix.set(1,2,5);
    }

    @Test
    public void create_matrix_from_two_dimensional_not_rectangular_array_should_correctly_set_data()
    {
        Matrix matrix = new Matrix(new double[][]{{1,2,3},{2},{1,2,3,4}});
        Assert.assertArrayEquals(matrix.asArray(),new double[][]{{1,2,3,0},{2,0,0,0},{1,2,3,4}});
    }

    @Test
    public void create_matrix_with_given_rows_and_columns_should_have_correct_size()
    {
        Matrix matrix = new Matrix(2,3);

        Assert.assertEquals(2,matrix.getRows());
        Assert.assertEquals(3,matrix.getColumns());
    }

    @Test
    public void asArray() {
        double[][] result = matrix.asArray();
        double[][] expected = new double[][]{{0,1,2},{3,4,5}};
        Assert.assertArrayEquals(expected,result);
    }

    @Test
    public void set_get_at_given_index_should_update_and_return_correct_data()
    {
        double result = matrix.get(1,2);

        Assert.assertEquals(result,5.0,0.00001);

        matrix.set(1,2,10);

        double result2 = matrix.get(1,2);

        Assert.assertEquals(result2,10,0.00001);
    }

    @Test
    public void toString_test()
    {
           String result = matrix.toString();
           String expected = "[[0.0,1.0,2.0],[3.0,4.0,5.0]]";
           Assert.assertEquals(expected,result);
    }

    @Test(expected = Exception.class)
    public void reshapre_should_throw_exception_if_new_size_is_incorrect(){
        matrix.reshape(3,3);
    }

    @Test
    public void dot()
    {
        Matrix a = new Matrix(new double[][]{{2,3},{4,5},{6,7}});
        /*
            2 3  9 8
            4 5  7 6
            6 7
        * */
        Matrix b = new Matrix(new double[][]{{9,8},{7,6}});
        double[][] expected = new double[][]{{39,34},{71,62},{103,90}};
        Matrix result = a.dot(b);

        Assert.assertArrayEquals(expected,result.asArray());
    }
    @Test
    public void Should_return_correct_frobenius_norm()
    {
        Matrix a = new Matrix(new double[][]{{9,8},{7,6}});
        //81 + 64 + 49 + 36
        //130 + 100 = 230
        double expectedResult = Math.sqrt(230);

        double result = a.fob();
        Assert.assertEquals(expectedResult,result,0.0001);
    }

    //Kartkowa Tests
    @Test
    public void Kartkowa_Test()
    {
        Matrix matrix = new Matrix(new double[][]{{1,2,3},{4,5,6,},{7,8,9},{10,11,12}});
        Matrix col = matrix.getColumn(1);
        System.out.println(col.toString());
    }
}