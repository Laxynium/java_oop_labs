package lab6.integration;

import lab6.CsvReader.CsvReader;
import org.junit.Test;

import java.io.IOException;
import java.io.StringReader;
import java.util.Optional;
import java.util.function.Function;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;

public class CsvReaderTests {

    @Test
    public void OnCvsFile() throws IOException {

        var reader = new CsvReader("test/lab6/integration/titanic-part.csv",",",true);

        assertThat(reader.getColumnLabels()).hasSize(12);

        while (reader.next()){
            for (int i = 0; i < reader.getRecordLength(); i++) {
                System.out.print(reader.get(i)+",");
            }
            System.out.println();
        }
    }
    @Test
    public void Test() throws IOException {

        String cvs = "C1,C2,C3,C4\nabc,345,34.35, ";
        var reader = new CsvReader(new StringReader(cvs),",",true);

        assertThat(reader.getColumnLabels()).hasSize(4).containsExactly("C1","C2","C3","C4");

        reader.next();
        assertThat(reader.getDouble(2)).isEqualTo(34.35);
        assertThat(reader.getInt(1)).isEqualTo(345);
        assertThat(reader.get(0)).isEqualTo("abc");
        assertThatThrownBy(()-> reader.getDouble(0))
                .isInstanceOf(Exception.class);
    }
    @Test
    public void NotExistingColumn() throws IOException {
        String cvs = "C1,C2,C3,C4\nabc,345,34.35, ";
        var reader = new CsvReader(new StringReader(cvs),",",true);
        reader.next();

        assertThatThrownBy(()->reader.get(3))
                .isInstanceOf(Exception.class);

        assertThat(reader.isMissing("C5")).isFalse();
    }
    @Test
    public void EmptyColumn() throws IOException {
        String cvs = "C1,C2,C3,C4\nabc,,34.35,a";
        var reader = new CsvReader(new StringReader(cvs),",",true);
        reader.next();

        assertThat(reader.isMissing((1))).isTrue();
        assertThat(reader.isMissing(("C2"))).isTrue();
        var x = reader.get(1);
    }

    private CsvReader reader;
    @Test
    public void SomeTest() throws IOException {
        String cvs = "C1,C2,C3,C4\nabc,,34.35,a";
        reader = new CsvReader(new StringReader(cvs),",",true);
        reader.next();

        Optional<Double> result = getIfOk(reader::getDouble,2);
    }

    private <T> Optional<T> getIfOk(Function<Integer,T>func, int index)
    {
        if(reader.isMissing(index))
            return Optional.empty();
        return Optional.of(func.apply(index));
    }

}
