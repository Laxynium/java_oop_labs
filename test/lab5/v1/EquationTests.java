package lab5.v1;

import lab5.v1.*;
import org.junit.Test;

import java.util.Locale;

import static org.assertj.core.api.Assertions.assertThat;

public class EquationTests {

    @Test
    public void Test(){
        Node node = new Sum(new Prod(new Constant(3),new Power(new Variable("x"),4)),
                new Sum(new Power(new Variable("x"),2),new Prod(new Constant(3),new Power(new Variable("x"),3))));

        Node derivative = node.diff(new Variable("x"));

        derivative.toString();
        assertThat(derivative.toString()).isEqualTo("3*4*x^3+2*x+3*3*x^2");
    }

    @Test
    public void Test2(){
        Variable x = new Variable("x");
        Node exp = new Sum()
                .add(2.1,new Power(x,3))
                .add(new Power(x,2))
                .add(-2,x)
                .add(7);
        assertThat(exp.toString()).isEqualTo("2.1*x^3+x^2+(-2)*x+7");
    }

    @Test
    public void diffPoly() {
        Variable x = new Variable("x");
        Node exp = new Sum()
                .add(2,new Power(x,3))
                .add(new Power(x,2))
                .add(-2,x)
                .add(7);
        System.out.print("exp=");
        System.out.println(exp.toString());

        assertThat(exp.toString()).isEqualTo("2*x^3+x^2+(-2)*x+7");

        Node d = exp.diff(x);
        System.out.print("d(exp)/dx=");
        System.out.println(d.toString());
    }
    @Test
    public  void buildAndEvaluate() {
        Variable x = new Variable("x");
        Node exp = new Sum()
                .add(new Power(x, 3))
                .add(-2, new Power(x, 2))
                .add(-1, x)
                .add(2);
        for (double v = -5; v < 5; v += 0.1) {
            x.setValue(v);
            System.out.printf(Locale.US, "f(%f)=%f\n", v, exp.evaluate());
        }
    }
}
