package lab5.v1;

import lab5.v1.*;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class PowerShould {

    @Test
    public void ReturnOneWhenExponentIs0(){
        Power power = new Power(new Constant(1),0);

        assertThat(power.toString()).isEqualTo("1");
    }

    @Test
    public void Return0WhenBaseIs0(){
        Power power = new Power(new Constant(0),3);

        assertThat(power.toString()).isEqualTo("0");
    }

    @Test
    public void ReturnBaseSurroundedWithParenthesesWhenBaseHaveMoreThanOneArgument(){
        Sum sum = new Sum(new Constant(1),new Constant(1)){
            //to make this test independent of sum implementation
            @Override
            int getArgumentsCount() {
                return 2;
            }
            @Override
            public String toString() {
                return "1+1";
            }
        };

        Power power = new Power(sum,3);

        assertThat(power.toString()).isEqualTo("(1+1)^3");
    }

    @Test
    public void ReturnBaseSurroundedWithParenthesesWhenBaseIsNegative(){
        Variable variable = new Variable("-x"){
            //to make test independent of Variable implementation
            @Override
            public String toString() {
                return "-x";
            }
        };
        Power power = new Power(variable,3);

        assertThat(power.toString()).isEqualTo("(-x)^3");
    }

    @Test
    public void ReturnVariableWithoutSurroundingParenthesesWhenBaseHaveLessThanTwoArguments(){
       Power power = new Power(new Variable("x"),3);

       assertThat(power.toString()).isEqualTo("x^3");
    }

    @Test
    public void CalculateCorrectDerivative(){
        Power power = new Power(new Variable("x"),3);

        assertThat(power.diff(new Variable("x"))).isEqualToComparingFieldByFieldRecursively(new Prod(3,new Power(new Variable("x"),2)).mul(new Constant(1)));
    }
}
