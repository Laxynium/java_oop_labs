package lab5.v1;

import lab5.v1.Constant;
import lab5.v1.Variable;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class VariableShould {

    @Test
    public void ReturnVariableWithGivenName()
    {
        Variable variable = new Variable("x");

        assertThat(variable.toString()).isEqualTo("x");
    }

    @Test
    public void ReturnVariableWithOutSignWhenItIsNotGivenPlus()
    {
        Variable variable = new Variable("x");

        assertThat(variable.toString()).isEqualTo("x");
    }

    @Test
    public void SetNegativeAndRemoveMinusFromNameWhenNameBeginsWithMinus()
    {
        Variable variable = new Variable("-x");

        assertThat(variable.isNegative).isTrue();
        assertThat(variable.toString()).isEqualTo("-x");
    }

    @Test
    public void ReturnVariableWithSignWhenItIsGivenMinus()
    {
        Variable variable = (Variable) (new Variable("x").negative());

        assertThat(variable.toString()).isEqualTo("-x");
    }

    @Test
    public void Return1WhenIsDifferentiated()
    {
        Variable variable = new Variable("x");

        assertThat(variable.diff(variable)).isEqualToComparingFieldByFieldRecursively(new Constant(1));
    }
    @Test
    public void ReturnMinus1WhenDifferentiatingNegativeVariable()
    {
        Variable variable = new Variable("-x");

        assertThat(variable.diff(variable)).isEqualToComparingFieldByFieldRecursively(new Constant(-1));
    }
}
