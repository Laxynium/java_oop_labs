package lab5.v1;

import lab5.v1.*;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ProdShould {

    @Test
    public void ReturnProdWithoutNumberOne()
    {
        Prod prod = new Prod(new Constant(1),new Variable("x"));

        assertThat(prod.toString()).isEqualTo("x");
    }
    @Test
    public void Return0WhenAtleastOneFactorIsZero()
    {
        Prod prod= new Prod(new Constant(0),new Variable("x"));

        assertThat(prod.toString()).isEqualTo("0");
    }

    @Test
    public void WrapNegativeNodeWithParentheses()
    {
        Prod prod = new Prod(new Constant(-5),new Variable("-x"))
                    .mul(new Variable("-y"));

        assertThat(prod.toString()).isEqualTo("(-5)*(-x)*(-y)");
    }

    @Test
    public void WrapNegatvieOrPositiveNodeWithArgumentsCountGreaterThan1WithParenetheses()
    {
        Prod prod = new Prod(new Sum(new Variable("-x"),new Variable("-y")).negative(),new Sum(new Constant(-5),new Variable("y")));

        assertThat(prod.toString()).isEqualTo("(-(-x-y))*(-5+y)");
    }
    @Test
    public void ReturnProdWithoutSurroundingParenthesesAndWithMinusAtTheBeginWhenItIsNegative()
    {
        Prod prod = (Prod)new Prod(new Prod(new Variable("-y"),new Variable("x")),new Sum(new Constant(-10),new Variable("y"))).negative();

        assertThat(prod.toString()).isEqualTo("-(-y)*x*(-10+y)");
    }

    @Test
    public void CalculateCorrentDerivative()
    {
        Node expceted = new Sum(
                    new Prod(new Prod(new Constant(3),
                                new Power(new Variable("x"),2))
                                .mul(new Constant(1)),
                                new Power(new Variable("x"),4)),
                    new Prod(new Power(new Variable("x"),3),
                            new Prod(new Constant(4),new Power(new Variable("x"),3)).mul(new Constant(1)))
                );
        Prod prod = new Prod(new Power(new Variable("x"),3),new Power(new Variable("x"),4));

        assertThat(prod.diff(new Variable("x"))).isEqualToComparingFieldByFieldRecursively(expceted);
    }
}
