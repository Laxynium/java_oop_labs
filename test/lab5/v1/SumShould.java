package lab5.v1;

import lab5.v1.*;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class SumShould {

    @Test
    public void ReturnArgumentsCountAsNumberOfGivenNodes()
    {
        Sum sum = new Sum(new Constant(1),new Constant(1))
                    .add(1).add(1);

        assertThat(sum.getArgumentsCount()).isEqualTo(4);
    }
    @Test
    public void ReturnSumWithReplacedPlusesWithMinusesWhenNodeIsNegative()
    {
        Sum sum = new Sum(new Constant(-1),new Constant(-1))
                    .add(new Constant(-1))
                    .add(new Constant(1));

        assertThat(sum.toString()).isEqualTo("-1-1-1+1");
    }
    @Test
    public void ReturnSumWithoutAnyParenthesesWhenNodesHaveNotMoreThan2Arguments()
    {
        Sum sum = new Sum(new Constant(1),new Constant(1));

        assertThat(sum.toString()).isEqualTo("1+1");
    }

    @Test
    public void ReturnSumWithoutZeros()
    {
        Sum sum = new Sum(new Constant(0),new Constant(0))
                .add(new Variable("-x"));

        assertThat(sum.toString()).isEqualTo("-x");
    }

    @Test
    public void WorkCorrectlyOnMoreComplexUseCase()
    {
        Sum sum = new Sum(new Variable("-x"),new Sum(new Variable("y"),new Variable("-x")).add(new Constant(-10))
            .add(new Sum(new Variable("-x"),new Constant(-5)).negative()));

        assertThat(sum.toString()).isEqualTo("-x+y-x-10-(-x-5)");
    }

    @Test
    public void ReturnSumSurroundedWithParenthesesAndWithMinusAtBeginWhenSumIsNegative(){
        Sum sum = (Sum)new Sum(new Variable("-x"),new Variable("-y")).negative();

        assertThat(sum.toString()).isEqualTo("-(-x-y)");
    }

    @Test
    public void ReturnSumOfDerivativesOfNodesInSumWhenIsDifferentiated()
    {
        Node expctedExpresion = new Sum(new Constant(-1),new Constant(0))
                .add(new Prod(new Constant(2),new Power(new Variable("x"),1)).mul(new Constant(1)));


        Sum sum = new Sum(new Variable("-x"),new Constant(5))
                    .add(new Power(new Variable("x"),2));


        Node result = sum.diff(new Variable("x"));

        assertThat(result).isEqualToComparingFieldByFieldRecursively(expctedExpresion);
    }
    @Test
    public void ReturnFlattenedString(){
        Sum expresion = new Sum(new Sum(new Variable("x"),new Variable("x")).add(3),
                new Sum(new Sum(new Constant(3),new Constant(3)),new Constant(3)));

        assertThat(expresion.toString()).isEqualTo("x+x+3+3+3+3");
    }
}
