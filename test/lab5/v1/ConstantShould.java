package lab5.v1;

import lab5.v1.Constant;
import lab5.v1.Variable;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ConstantShould {

    @Test
    public void ReturnNumberWith5DigitsAfterDot(){
        double value = 0.111111;
        Constant constant = new Constant(value);

        assertThat(constant.toString()).isEqualTo("0.11111");
    }

    @Test()
    public void ReturnNumberWithOutSignAndZerosAfterDotWhenValueIsGreaterOrEqual0(){
        double value = 1.0;
        Constant constant = new Constant(value);

        assertThat(constant.toString()).isEqualTo("1");
    }

    @Test()
    public void ReturnNumberWithSignWhenValueIsLessThan0(){
        double value = -3.0;
        Constant constant = new Constant(value);

        assertThat(constant.toString()).isEqualTo("-3");
    }

    @Test()
    public void Return0WhenIsDifferentited()
    {
        Constant constant = new Constant(-3);

        assertThat(constant.diff(new Variable("x"))).isEqualToComparingFieldByFieldRecursively(new Constant(0));
    }
}
