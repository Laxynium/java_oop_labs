package lab5.v2;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ProdTests {
    @Test
    public void EvaluatesCorrectValue(){
        var variable = new Variable("x");
        Prod prod = Prod.builder()
                .multiplyBy(3)
                .multiplyBy(variable)
                .multiplyBy(9)
                .multiplyBy(variable)
                .build();
        variable.setValue(3.5);
        assertThat(prod.evaluate()).isEqualTo(330.75);
    }
    @Test
    public void OnTwoOnes(){
        var prod = Prod.builder()
                    .multiplyBy(1)
                    .multiplyBy(1)
                .build();
        System.out.println(prod.toString());
        var diff = prod.diff(new Variable("x"));
        System.out.println(diff);
        var sum = Sum.builder()
                .add(3)
                .add(Prod.builder().multiplyBy(1).multiplyBy(1).build())
                .build();

        System.out.println(sum.toString());
    }
}
