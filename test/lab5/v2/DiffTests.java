package lab5.v2;

import javafx.util.Pair;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Locale;

import static org.assertj.core.api.Assertions.assertThat;

public class DiffTests {
    @Test
    public void OnExpression() {
        var sum = Sum.builder()
                .add(new Power("x", 3))
                .add(Prod.builder()
                        .multiplyBy(3)
                        .multiplyBy(new Power("x", 2))
                        .multiplyBy(Sum.builder()
                                .add("x")
                                .add(new Power("x", 4))
                                .build())
                        .build())
                .build();
        System.out.println(sum.toString());
        var result = sum.diff(new Variable("x"));
        System.out.println(result);

    }
    @Test
    public  void diffPoly() {
        Variable x = new Variable("x");
        Node exp = Sum.builder()
                .add(new Prod(new Constant(2),new Power(x,3)))
                .add(new Power(x,2))
                .add(new Prod(new Constant(-2),x))
                .add(7)
                .build();
        System.out.print("exp=");
        System.out.println(exp.toString());

        Node d = exp.diff(x);
        System.out.print("d(exp)/dx=");
        System.out.println(d.toString());

    }
    @Test
    public void diffCircle() {
        Variable x = new Variable("x");
        Variable y = new Variable("y");
        Node circle = Sum.builder()
                .add(new Power(x,2))
                .add(new Power(y,2))
                .add(new Prod(new Constant(8),x))
                .add(new Prod(new Constant(4),y))
                .add(16)
                .build();
        System.out.print("f(x,y)=");
        System.out.println(circle.toString());

        Node dx = circle.diff(x);
        System.out.print("d f(x,y)/dx=");
        System.out.println(dx.toString());
        System.out.print("d f(x,y)/dy=");
        Node dy = circle.diff(y);
        System.out.println(dy.toString());
    }

}
