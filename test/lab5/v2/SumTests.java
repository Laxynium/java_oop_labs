package lab5.v2;

import org.junit.Test;

import java.util.ArrayList;

import static org.assertj.core.api.Assertions.*;

public class SumTests {

    @Test
    public void ThrowsExceptionWhenLessThan2Arguments(){
        assertThatThrownBy(()->Sum.builder().add(3).build()).isInstanceOf(Exception.class);
    }

    @Test
    public void evaluateCorrectValue(){
        var variable = new Variable("x");
        var sum = Sum.builder().add(3.0)
                .add(variable)
                .add(Sum.builder()
                        .add(4)
                        .add(variable).build()).build();
        variable.setValue(5);
        assertThat(sum.evaluate()).isEqualTo(17);
    }
    @Test
    public void diffOnTwoZeros(){
        var sum = Sum.builder()
                    .add(0)
                    .add(0)
                    .build();
        var diff = sum.diff(new Variable("x"));
        System.out.println(diff);
    }
    @Test
    public void diffOnZeroAndVariable(){
        var sum = Sum.builder()
                .add(0)
                .add("x")
                .build();
        var diff = sum.diff(new Variable("x"));
        System.out.println(diff);
    }
}
