package lab5.v2;

import javafx.util.Pair;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Locale;

public class Tests {
    @Test
    public void OnExpression() {
        var sum = Sum.builder()
                .add(new Power("x", 3))
                .add(Prod.builder()
                        .multiplyBy(3)
                        .multiplyBy(new Power("x", 2))
                        .multiplyBy(Sum.builder()
                                .add("x")
                                .add(new Power("x", 4))
                                .build())
                        .build())
                .build();
        System.out.println(sum.toString());
        var result = sum.diff(new Variable("x"));
        System.out.println(result);

    }

    @Test
    public void ExpressionWithMinus() {
        var prod = Prod.builder().multiplyBy(1).multiplyBy(3).build();
        System.out.println(prod.toString());

        var sum = new Sum(new Prod(new Constant(-1), new Variable("x")),
                Prod.builder()
                        .multiplyBy(-3)
                        .multiplyBy("y")
                        .multiplyBy(9)
                        .build());

        System.out.println(sum);
    }

    @Test
    public void buildAndPrint() {
        Variable x = new Variable("x");
        Node exp = Sum.builder()
                .add(new Prod(new Constant(2.1), new Power(x, 3)))
                .add(new Power(x, 2))
                .add(new Prod(new Constant(-2), x))
                .add(7)
                .build();
        System.out.println(exp.toString());
    }

    @Test
    public void buildAndEvaluate() {
        Variable x = new Variable("x");
        Node exp = Sum.builder()
                .add(new Power(x, 3))
                .add(new Prod(new Constant(-2), new Power(x, 2)))
                .add(new Prod(new Constant(-1), x))
                .add(2)
                .build();
        for (double v = -5; v < 5; v += 0.1) {
            x.setValue(v);
            System.out.printf(Locale.US, "f(%f)=%f\n", v, exp.evaluate());
        }
    }

    @Test
    public void defineCircle() {
        Variable x = new Variable("x");
        Variable y = new Variable("y");
        Node circle = Sum.builder()
                .add(new Power(x, 2))
                .add(new Power(y, 2))
                .add(new Prod(new Constant(8), x))
                .add(new Prod(new Constant(4), y))
                .add(16)
                .build();
        System.out.println(circle.toString());

        double xv = 100 * (Math.random() - .5);
        double yv = 100 * (Math.random() - .5);
        x.setValue(xv);
        y.setValue(yv);
        double fv = circle.evaluate();
        System.out.print(String.format("Punkt (%f,%f) leży %s koła %s", xv, yv, (fv < 0 ? "wewnątrz" : "na zewnątrz"), circle.toString()));
    }
    @Test
    public void pointsInsideCircle(){
        Variable x = new Variable("x");
        Variable y = new Variable("y");
        Node circle = Sum.builder()
                .add(new Power(x, 2))
                .add(new Power(y, 2))
                .add(new Prod(new Constant(8), x))
                .add(new Prod(new Constant(4), y))
                .add(16)
                .build();
        System.out.println(circle.toString());

        //x <-6,-2>, y <0,-4>
        var inside = new ArrayList<Pair<Double,Double>>();
        boolean done = false;
        for(double i = -6 ; i<=-2 ; i+=0.05) {
            for (double j = -4; j <= 0; j+=0.05) {
                x.setValue(i);
                y.setValue(j);
                double fv = circle.evaluate();
                if(fv < 0)
                    inside.add(new Pair<>(i, j));
                if(inside.size() >= 100){
                    done = true;
                    break;
                }
            }
            if(done)
                break;
        }
        inside.forEach(p-> System.out.println(String.format("Point (%s,%s)",p.getKey(),p.getValue())));
    }

}
