package lab2;

//import com.sun.javaws.exceptions.InvalidArgumentException;

import java.util.Arrays;
import java.util.Comparator;
import java.util.function.BiConsumer;

public class Matrix {

    private double[]data;
    private int rows;
    private int columns;

    Matrix(int rows, int columns){

        this.rows = rows;
        this.columns = columns;
        data = new double[rows*columns];
    }
    Matrix(double[][] elements){
        this.rows = elements.length;

        int longestRow = Arrays.stream(elements)
                .max(Comparator.comparingInt(x -> x.length))
                .map(x->x.length)
                .get();

        this.columns = longestRow;

        this.data = new double[this.rows*this.columns];
        for(int i = 0 ; i < this.rows ; ++i)
        {
            for(int j = 0 ; j <this.columns ; ++j)
            {
                if(j >= elements[i].length)
                    this.data[getIndex(i,j)] = 0;
                else
                    this.data[getIndex(i,j)] = elements[i][j];
            }
        }
    }

    public int getRows() {
        return rows;
    }

    public int getColumns() {
        return columns;
    }

    double[][] asArray()
    {
        double[][]result = new double[this.rows][this.columns];

        for(int i = 0; i < this.rows; ++i)
        {
            for (int j = 0 ;j < this.columns; ++j)
            {
                result[i][j] = this.data[getIndex(i,j)];
            }
        }
        return result;
    }
    public double get(int row, int column)
    {
        return this.data[getIndex(row,column)];
    }
    public void set(int row,int column,double value)
    {
        this.data[getIndex(row,column)] = value;
    }

    void reshape(int newRows, int newColumns)
    {
        if(this.rows * this.columns != newRows * newColumns)
            throw new IllegalArgumentException("");

        this.rows = newRows;
        this.columns = newColumns;
    }

    public int[] shape()
    {
        return new int[]{this.rows,this.columns};
    }

    Matrix add(Matrix m)
    {
        validateNumberOfRowsAndColumns(m);
        Matrix matrix = copy();
        iterate((value,index)->{matrix.data[index] += m.data[index];});
        return matrix;
    }

    Matrix sub(Matrix m)
    {
        validateNumberOfRowsAndColumns(m);
        Matrix matrix = copy();
        iterate((value,index)->{matrix.data[index] -= m.data[index];});
        return matrix;
    }

    Matrix mul(Matrix m)
    {
        validateNumberOfRowsAndColumns(m);
        Matrix matrix = copy();
        iterate((value,index)->{matrix.data[index] *= m.data[index];});
        return matrix;
    }

    Matrix div(Matrix m)
    {
        validateNumberOfRowsAndColumns(m);
        Matrix matrix = copy();
        iterate((value,index)->{matrix.data[index] /= m.data[index];});
        return matrix;
    }

    Matrix add(double value)
    {
        Matrix matrix = copy();
        Arrays.stream(matrix.data).forEach(x->x+=value);
        return matrix;
    }

    Matrix sub(double value)
    {
        Matrix matrix = copy();
        Arrays.stream(matrix.data).forEach(x->x-=value);
        return matrix;
    }

    Matrix mul(double value)
    {
        Matrix matrix = copy();
        Arrays.stream(matrix.data).forEach(x->x*=value);
        return matrix;
    }

    Matrix div(double value)
    {
        if(value == 0)
            throw new IllegalArgumentException("Value cannot be null");
        Matrix matrix = copy();
        Arrays.stream(matrix.data).forEach(x->x/=value);
        return matrix;
    }

    Matrix dot(Matrix other)
    {
        if(this.columns != other.rows)
            throw new IllegalArgumentException("Column count of left matrix must be equal to row count of the right matrix");

        Matrix matrix = new Matrix(this.rows,other.columns);

        for(int i = 0 ; i < matrix.rows; ++i)
            for(int j = 0 ; j < matrix.columns; ++j)
            {
                double sum = 0;
                for(int k =0 ;k < this.columns; ++k)
                {
                   sum += this.data[getIndex(i,k)] * other.data[getIndex(k,j)];
                }
                matrix.data[getIndex(i,j)] = sum;
            }
        return matrix;
    }

    double fob()
    {
        return Math.sqrt(Arrays.stream(this.data).map(x->x*x).sum());
    }

    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder();

        builder.append("[");

        for(int i = 0; i < this.rows; ++i)
        {
            builder.append("[");
            for (int j = 0 ;j < this.columns; ++j)
            {
                builder.append(this.data[getIndex(i,j)]);
                if(j < this.columns-1)
                    builder.append(",");
            }
            builder.append("]");
            if(i < this.rows - 1)
                builder.append(",");
        }
        builder.append("]");

        return builder.toString();
    }

    //Kartkówka
    public Matrix getColumn(int index)
    {
        if(index < 0 || index >= this.getColumns())
            throw new IllegalArgumentException("Argument is out of range.");

        Matrix matrix = new Matrix(this.getRows(),1);

        for(int i =0 ;i<this.getRows();++i)
        {
            matrix.data[matrix.getIndex(i,0)] = this.data[this.getIndex(i,index)];
        }

        return matrix;
    }
    //

    private void iterate(BiConsumer<Double,Integer> onElement)
    {
        for(int i = 0; i < this.data.length; ++i)
        {
            onElement.accept(this.data[i],i);
        }
    }

    private Matrix copy()
    {
        Matrix matrix = new Matrix(this.rows,this.columns);
        for(int i = 0 ;i < this.data.length; ++i)
            matrix.data[i] = this.data[i];
        return matrix;
    }

    private void validateNumberOfRowsAndColumns(Matrix other)
    {
        if(other.columns != this.columns && other.rows != this.rows)
            throw new IllegalArgumentException("Matrix must have the same amount of rows and columns.");
    }

    private int getIndex(int row,int column)
    {
        return row*this.columns + column;
    }
}
