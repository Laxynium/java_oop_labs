package lab2;

public class Program {

    public static void main(String[]args)
    {
        Matrix matrix = new Matrix(new double[][]{{1,2,3},{3,4,5}});
        double[][]asArray = matrix.asArray();
        System.out.println(matrix);
        matrix.reshape(1,6);
        System.out.println(matrix);
    }
}
