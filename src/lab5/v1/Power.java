package lab5.v1;

public class Power extends Node{

    private final Node base;
    private final double exponent;

    public Power(Node base , double exponent) {

        this.base = base;
        this.exponent = exponent;
    }

    @Override
    double evaluate() {
        double argVal = base.evaluate();
        return Math.pow(argVal, exponent);
    }
    int getArgumentsCount(){return 1;}

    @Override
    Node diff(Variable variable) {
        return new Prod(
                    new Constant(this.exponent),
                    new Power(this.base,this.exponent -1))
                .mul(this.base.diff(variable));
    }

    @Override
    boolean isZero() {
        return base.isZero();
    }

    @Override
    public String toString() {
        if(exponent == 0)
            return "1";
        if(base.isZero())
            return "0";

        StringBuilder builder = new StringBuilder();

        if(baseNeedsParentheses())
            builder.append("(");

        builder.append(base.toString());

        if(baseNeedsParentheses())
            builder.append(")");

        if(exponent != 1)
            builder.append("^").append(new Constant(exponent).toString());

        return builder.toString();
    }
    private boolean baseNeedsParentheses(){
        return base.getArgumentsCount() > 1 || base.isNegative;
    }
}
