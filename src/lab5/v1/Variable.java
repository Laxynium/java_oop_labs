package lab5.v1;

import java.util.Optional;

public class Variable extends Node{
    private final String name;
    private Optional<Double> value;

    Variable(String name){
        if(name.isEmpty()) throw new IllegalArgumentException("name cannot be empty");
        if(name.charAt(0)=='-'){
            isNegative = true;
            this.name = name.substring(1);
        }else{
            this.name = name;
        }
    }

    public void setValue(Double value) {
        this.value = Optional.of(value);
    }

    @Override
    double evaluate() {
        return value.orElseThrow(()->new IllegalStateException("You must specify first value."));
    }

    @Override
    Node diff(Variable variable) {
        if(!this.name.equals(variable.name))
            return this;
        return new Constant(isNegative? -1 : 1);
    }

    @Override
    boolean isZero() {
        return false;
    }

    @Override
    public String toString() {
        return isNegative ? "-" + name : name;
    }
}
