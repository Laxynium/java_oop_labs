package lab5.v1;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

public final class Constant extends Node{
    private final double value;
    Constant(double value){
        this.value = value;
        this.isNegative  = this.value < 0;
    }

    public double getValue() {
        return value;
    }

    @Override
    double evaluate() {
        return value;
    }

    @Override
    Node diff(Variable variable) {
        return new Constant(0);
    }

    @Override
    boolean isZero() {
        return value == 0;
    }

    @Override
    public String toString() {
        DecimalFormat format = new DecimalFormat("0.#####",new DecimalFormatSymbols(Locale.US));
        return format.format(value);
    }
}
