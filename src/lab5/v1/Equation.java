package lab5.v1;

public class Equation {
    public static void main(String[]args){
        Sum sum = new Sum(new Power(new Variable("x"), 3), new Variable("x"));
        sum.add(new Prod(3,new Variable("x")));
        String result = sum.toString();
        System.out.println(result);
        Node diff = sum.diff(new Variable("x"));
        String diffResult = diff.toString();
        System.out.println(diffResult);
    }

}
