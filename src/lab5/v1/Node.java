package lab5.v1;

public abstract class Node {
    protected boolean isNegative = false;
    Node positive(){
        isNegative = false;
        return this;
    }
    Node negative(){
        isNegative = true;
        return this;
    }
    int getArgumentsCount(){return 1;}
    abstract double evaluate();
    @Override
    public abstract String toString();

    abstract Node diff(Variable variable);

    abstract boolean isZero();
}