package lab5.v1;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

class Prod extends Node {
    List<Node> arguments = new ArrayList<>();

    Prod(){}

    Prod(Node node){
        arguments.add(node);
    }
    Prod(double constant){
        arguments.add(new Constant(constant));
    }

    Prod(Node node1, Node node2){
       insertNode(node1);
       insertNode(node2);
    }
    Prod(double constant, Node node){
        this(new Constant(constant),node);
    }

    Prod mul(Node n){
        insertNode(n);
        return this;
    }

    Prod mul(double constant){
        arguments.add(new Constant(constant));
        return this;
    }

    @Override
    double evaluate() {
        return   arguments.stream()
                .mapToDouble(x->x.evaluate())
                .reduce(1,(x,y)->x*y);
    }

    @Override
    int getArgumentsCount() {
        return 1;
//        return (int)arguments.stream().filter(x->!(x instanceof Constant) || ((Constant)x).getValue()!=1).count();
    }

    @Override
    Node diff(Variable variable) {
        Sum sum = new Sum();
        for (int i = 0; i < arguments.size(); i++) {
            Prod prod = new Prod();
            for (int j = 0; j < arguments.size(); j++) {
                Node arg = arguments.get(j);
                if(i==j)
                    prod.mul(arg.diff(variable));
                else
                    prod.mul(arg);
            }
            sum.add(prod);
        }
        return sum;
    }

    @Override
    boolean isZero() {
        return arguments.stream().anyMatch(x -> x instanceof Constant && ((Constant) x).getValue() == 0);
    }

    @Override
    public String toString(){
        StringBuilder b =  new StringBuilder();

        if(isZero())
            return "0";

        if(isNegative)
            b.append("-");

        b.append(arguments.stream()
                .filter(x->!(x instanceof Constant) || ((Constant)x).getValue()!=1)
                .map(x->{
                    if(x.isNegative || x.getArgumentsCount() > 1)
                        return "(" + x.toString() + ")";
                    return x.toString();
                })
                .collect(Collectors.joining("*")));

        return b.toString();
    }
    private void insertNode(Node node)
    {
        if(node instanceof Prod && !node.isNegative){
            arguments.addAll(((Prod) node).arguments);
        }else{
            arguments.add(node);
        }
    }
}
