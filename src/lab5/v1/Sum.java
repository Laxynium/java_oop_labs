package lab5.v1;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Sum extends Node {

    List<Node> arguments = new ArrayList<>();

    Sum(){}

    Sum(Node arg1, Node arg2){
        insertNode(arg1);
        insertNode(arg2);
    }


    Sum add(Node n){
        insertNode(n);
        return this;
    }

    Sum add(double c){
        arguments.add(new Constant(c));
        return this;
    }

    Sum add(double c, Node n) {
        Node mul = new Prod(c,n);
        arguments.add(mul);
        return this;
    }

    @Override
    double evaluate() {
        return   arguments.stream()
                .mapToDouble(x->x.evaluate())
                .sum();
    }

    @Override
    Node diff(Variable variable) {
        Sum sum =  new Sum();
        for (Node arg : this.arguments) {
            sum.add(arg.diff(variable));
        }
        return sum;
    }

    @Override
    int getArgumentsCount() {
        return (int)arguments.stream().filter(x->!x.isZero()).count();
    }

    @Override
    boolean isZero() {
        return arguments.stream().allMatch(x->x.isZero());
    }

    @Override
    public String toString(){
        StringBuilder builder =  new StringBuilder();

        if(isNegative)
            builder.append("-").append("(");

        List<Node> elements = arguments.stream().filter(x -> !x.isZero()).collect(Collectors.toList());

        for (int i = 0; i < elements.size(); i++) {
            Node x = elements.get(i);

            String s = null;
            if(x.getArgumentsCount() > 1 && !x.isNegative)
                s = "(" + x.toString() + ")";
            else
                s = x.toString();
            if (x.isNegative) {
                builder.append(s);
            } else {
                if(i != 0 )
                    builder.append("+");
                builder.append(s);
            }
        }
        if(isNegative)
            builder.append(")");
        return builder.toString();
    }
    private void insertNode(Node node){
        if(node instanceof Sum && !node.isNegative){
            arguments.addAll(((Sum) node).arguments);
        }else{
            arguments.add(node);
        }
    }
}
