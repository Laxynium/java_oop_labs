package lab5.v2;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Sum extends Node {

    private List<Node> arguments = new ArrayList<>();

    public static Builder builder(){
        return new Builder();
    }

    private Sum(){
    }
    public Sum(Node left,Node right){
        add(left);
        add(right);
    }
    private void add(Node node){
        if(node instanceof Sum){
            ((Sum) node).arguments.forEach(x->add(x));
            return;
        }
        if(node.isZero())
            return;
        arguments.add(node);
    }

    @Override
    protected boolean isZero() {
        return arguments.stream().allMatch(x->x.isZero());
    }

    @Override
    public double evaluate() {
        return arguments.stream()
                .mapToDouble(x->x.evaluate())
                .sum();
    }

    @Override
    public Node diff(Variable variable) {
        var builder = Sum.builder();
        getArguments().stream().forEach(x->{
            var diff = x.diff(variable);
                builder.add(diff);
        });
        return builder.build();
    }
    private List<Node>getArguments(){
        var parsedArguments = new ArrayList<>(arguments);
        if(parsedArguments.size() <=1){
            for(int i = 0 ; i < 2-arguments.size();++i){
                parsedArguments.add(new Constant(0));
            }
        }
        return parsedArguments;
    }
    @Override
    public String toString() {
        if(arguments.isEmpty())
            return "(0)";
        return arguments.stream()
                .map(x->x.toString())
                .collect(Collectors.joining("+","(",")"));
    }

    public static class Builder{
        private Sum sum = new Sum();
        private int added =0;
        public Builder add(String variable){
            return add(new Variable(variable));
        }
        public Builder add(double value){
            return add(new Constant(value));
        }
        public Builder add(Node node){
            sum.add(node);
            ++added;
            return this;
        }
        public Sum build(){
            if(added <2)
                throw new IllegalStateException("You must specify at least 2 arguments");
            return sum;
        }
    }
}
