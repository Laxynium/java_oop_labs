package lab5.v2;

import java.util.Optional;

public class Variable extends Node {
    Optional<Double> value = Optional.empty();
    private String name;

    public Variable(String name){
        this.name = name;
    }
    public void setValue(double value){
        this.value = Optional.of(value);
    }

    @Override
    protected boolean isZero() {
        return false;
    }

    @Override
    public double evaluate() {
        return value.orElseThrow();
    }

    @Override
    public Node diff(Variable variable) {
        if(variable.name != this.name)
            return new Constant(0);
        return new Constant(1);
    }

    @Override
    public String toString() {//always positive x
        return name;
    }
}
