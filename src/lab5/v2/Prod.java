package lab5.v2;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Prod extends Node {

    private List<Node> arguments = new ArrayList<>();

    public static Builder builder(){
        return new Builder();
    }
    private Prod(){}
    public Prod(Node left,Node right){
        add(left);
        add(right);
    }
    private void add(Node node){
        if(node instanceof Prod){
            ((Prod) node).arguments.forEach(x->add(x));
            return;
        }
        if (node instanceof Constant) {
            if(((Constant)node).isOne())
                return;
            if(node.isZero())
                arguments.clear();
        }
        arguments.add(node);
    }
    @Override
    protected boolean isZero() {
        return arguments.stream().anyMatch(x->x.isZero());
    }

    @Override
    protected boolean isNegative() {
        var countOfNegatives = arguments.stream().filter(x->x.isNegative()).count();
        return countOfNegatives % 2 != 0 ;
    }

    @Override
    public double evaluate() {
        return arguments.stream()
                .mapToDouble(x->x.evaluate())
                .reduce(1,(x,y)->x*y);
    }

    @Override
    public Node diff(Variable variable) {
        Sum.Builder sum = Sum.builder();

        var newArguments = getArguments();

        for (int i = 0; i < newArguments.size(); i++) {
            Prod.Builder prod = Prod.builder();
            for (int j = 0; j < newArguments.size(); j++) {
                Node arg = newArguments.get(j);
                if(i==j)
                    prod.multiplyBy(arg.diff(variable));
                else
                    prod.multiplyBy(arg);
            }
            sum.add(prod.build());
        }
        return sum.build();
    }
    private List<Node> getArguments(){
        var parsedArguments = new ArrayList<>(arguments);
        if(parsedArguments.size() <=1){
            for(int i = 0 ; i < 2-arguments.size();++i){
                parsedArguments.add(new Constant(1));
            }
        }
        return parsedArguments;
    }
    @Override
    public String toString() {
        var builder = new StringBuilder();

        builder.append("(");

        if(this.isNegative())
            builder.append("-");

        if(arguments.isEmpty()){
            return "(1)";
        }
        if(arguments.size()==1 && arguments.get(0) instanceof Constant && arguments.get(0).evaluate()==-1){
            return "(-1)";
        }

        arguments.forEach(x->{
            if(x.isNegative() && x instanceof Constant)
            {
                if(x.evaluate() == -1)
                    return;
                else{
                    builder.append(new Constant(-x.evaluate()));
                }
            }else{
                builder.append(x);
            }
            builder.append("*");
        });
        var lastIndex = builder.lastIndexOf("*");
        if(lastIndex!=-1)
            builder.deleteCharAt(lastIndex);


        builder.append(")");

        return builder.toString();
    }

    public static class Builder{
        private Prod prod = new Prod();
        private int added = 0;

        public Builder multiplyBy(double value){
            return multiplyBy(new Constant(value));
        }
        public Builder multiplyBy(String variable){
            return multiplyBy(new Variable(variable));
        }
        public Builder multiplyBy(Node node){
            prod.add(node);
            ++added;
            return this;
        }
        public Prod build(){
            if(added < 2)
                throw new IllegalStateException("You must specify at least 2 arguments");
            return prod;
        }
    }
}
