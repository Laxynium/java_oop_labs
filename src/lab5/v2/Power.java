package lab5.v2;

public class Power extends Node{

    private final Node base;
    private final Constant exponent;

    public Power(String base,double exponent){
        this(new Variable(base),exponent);
    }

    public Power(Variable base,double exponent){
        this(base,new Constant(exponent));
    }
    public Power(Node base,double exponent){
        this(base,new Constant(exponent));
    }
    public Power(Node base, Constant exponent){
        this.base = base;
        this.exponent = exponent;
    }

    @Override
    protected boolean isZero() {
        return base.isZero();
    }

    @Override
    public double evaluate() {
        return Math.pow(base.evaluate(),exponent.evaluate());
    }

    @Override
    public Node diff(Variable variable) {

        Prod prod = Prod.builder()
                .multiplyBy(exponent)
                .multiplyBy(new Power(this.base,exponent.evaluate()-1))
                .multiplyBy(base.diff(variable))
                .build();
        return prod;
    }

    @Override
    public String toString() {
        if(exponent.isOne()){
            return base.toString();
        }
        if(exponent.isZero()){
            return "1";
        }
        var negative = base.isNegative();
        var builder = new StringBuilder();
        if(negative)
            builder.append("(");

            builder.append(base);

        if(negative)
            builder.append(")");

        builder.append("^").append(exponent);

        return builder.toString();
    }
}
