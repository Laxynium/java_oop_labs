package lab5.v2;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

public class Constant extends Node {

    private double value;

    public boolean isOne(){
        return value==1;
    }
    public Constant(double value){
        this.value = value;
    }

    @Override
    protected boolean isZero() {
        return value == 0;
    }

    @Override
    protected boolean isNegative(){return value < 0;}

    @Override
    public double evaluate() {
        return value;
    }

    @Override
    public Node diff(Variable variable) {
        return new Constant(0);
    }

    @Override
    public String toString() {//-c or c
        var format = new DecimalFormat("0.#####",new DecimalFormatSymbols(Locale.US));
        return format.format(value);
    }
}
