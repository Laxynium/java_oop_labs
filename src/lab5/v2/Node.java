package lab5.v2;

public abstract class Node {
    protected abstract boolean isZero();
    protected boolean isNegative(){return false;}
    public abstract double evaluate();
    public abstract Node diff(Variable variable);
    @Override
    public  abstract String toString();
}

