package lab4.v1;

import java.io.OutputStream;
import java.io.PrintStream;

public class Picture implements Element {
    private final String url;

    public Picture(String url){
        this.url = url;
    }

    @Override
    public void writeHTML(OutputStream stream) {
        PrintStream out = new PrintStream(stream);
        out.printf("<img alt='Some picture' src='%s' height='42' width='42'/>",this.url);
    }
}
