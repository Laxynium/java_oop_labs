package lab4.v1;

import java.io.ByteArrayOutputStream;

public class Program {
    public static void main(String[]args) throws Exception {
        Document cv = Document.builder()
                .withTitle("Jan Kowalski - CV")
                .withPhoto(new Picture("https://dsa.com"))
                .addSection(Section.builder()
                    .addParagraph(new Paragraph("2000-2005 Przedszkole im. Króleny Snieżki w..."))
                    .addParagraph(new Paragraph("2006-2012 im Ronalda Regana w ..."))
                    .addParagraph(new Paragraph("..."))
                    .build())
                .addSection(Section.builder()
                    .withTitle("Umiejętności")
                    .addParagraph(ParagraphWithList.builder()
                            .withText("Umiejętności")
                            .withList(UnorderedList.builder()
                                .addItem(new ListItem("C"))
                                .addItem(new ListItem("C++"))
                                .addItem(new ListItem("Java"))
                                .build())
                            .build())
                        .build())
                .build();

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        cv.writeHTML(stream);
        String result = stream.toString();
        System.out.println(result);
    }
}
