package lab4.v1;

import lab4.framework.BuilderBase;
import lab4.framework.NestedBuilder;

import java.io.OutputStream;
import java.io.PrintStream;
import java.util.function.Consumer;

public class ParagraphWithList extends Paragraph {

    private UnorderedList list;

    private ParagraphWithList(){
        super("");
    }

    public static Builder builder() throws Exception {
        return new Builder();
    }

    @Override
    public void writeHTML(OutputStream stream) {
        PrintStream out = new PrintStream(stream);

        out.printf("<p>%s</p>",text);
        list.writeHTML(out);
    }
    public static class Builder extends BuilderBase<ParagraphWithList> {
        public Builder() throws Exception {
            super(ParagraphWithList.class);
        }

        ParagraphWithList.Builder withText(String text)
        {
            instance.text = text;
            return this;
        }
        ParagraphWithList.Builder withList(UnorderedList list)
        {
            this.instance.list = list;
            return this;
        }
    }
}
