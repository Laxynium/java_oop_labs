package lab4.v1;

import lab4.framework.BuilderBase;
import lab4.framework.NestedBuilder;

import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class Section implements Element {
    private  String title;
    private  List<Paragraph> paragraphs = new ArrayList<>();

    public static Builder builder() throws Exception {
        return new Builder();
    }

    private Section(){
    }
    @Override
    public void writeHTML(OutputStream stream) {
        PrintStream out = new PrintStream(stream);
        out
            .append("<h2>")
            .append(this.title)
            .append("</h2>");

        paragraphs.forEach(x->x.writeHTML(out));
    }
    public static class Builder extends BuilderBase<Section> {
        Builder() throws Exception {
            super(Section.class);
        }
        Builder withTitle(String title)
        {
            instance.title = title;
            return this;
        }

        Builder addParagraph(Paragraph paragraph)
        {
            this.instance.paragraphs.add(paragraph);
            return this;
        }
    }
}
