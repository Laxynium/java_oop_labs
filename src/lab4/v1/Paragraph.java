package lab4.v1;

import java.io.OutputStream;
import java.io.PrintStream;

public class Paragraph implements Element {
    protected String text;

    Paragraph(String text){
        this.text = text;
    }

    @Override
    public void writeHTML(OutputStream stream) {
        PrintStream out = new PrintStream(stream);
        out.printf("<p>%s</p>",text);
    }
}
