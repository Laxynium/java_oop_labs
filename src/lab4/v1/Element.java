package lab4.v1;

import java.io.OutputStream;

public interface Element {
    void writeHTML(OutputStream stream);
}
