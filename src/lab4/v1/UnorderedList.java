package lab4.v1;

import lab4.framework.BuilderBase;
import lab4.framework.NestedBuilder;

import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class UnorderedList implements Element {
    private  List<ListItem> items = new ArrayList<>();

    public static UnorderedList.Builder builder() throws Exception {
        return new Builder();
    }

    private UnorderedList(){
    }

    @Override
    public void writeHTML(OutputStream stream) {
        PrintStream out = new PrintStream(stream);
        out.append("<ul>");
        items.forEach(x->x.writeHTML(stream));
        out.append("</ul>");
    }
    static class Builder extends BuilderBase<UnorderedList> {
        Builder() throws Exception {
            super(UnorderedList.class);
        }
        Builder addItem(ListItem item)
        {
            instance.items.add(item);
            return this;
        }
    }
}
