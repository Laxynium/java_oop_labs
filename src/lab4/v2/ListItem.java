package lab4.v2;

import java.io.OutputStream;
import java.io.PrintStream;

public class ListItem implements Element {
    private String content;

    public ListItem(String content){
        this.content = content;
    }
    @Override
    public void writeHTML(OutputStream stream) {
        PrintStream out = new PrintStream(stream);
        out.printf("<li>%s</li>",content);
    }
}
