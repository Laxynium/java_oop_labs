package lab4.v2;

import java.io.OutputStream;

public interface Element {
    void writeHTML(OutputStream stream);
}
