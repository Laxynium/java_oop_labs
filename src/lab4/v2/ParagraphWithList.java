package lab4.v2;

import lab4.framework.NestedBuilder;

import java.io.OutputStream;
import java.io.PrintStream;
import java.util.function.Consumer;

public class ParagraphWithList extends Paragraph {

    private UnorderedList list;

    private ParagraphWithList(){
        super("");
    }

    @Override
    public void writeHTML(OutputStream stream) {
        PrintStream out = new PrintStream(stream);

        out.printf("<p>%s</p>",text);
        list.writeHTML(out);
    }
    public static class Builder extends NestedBuilder<Section.Builder,ParagraphWithList> {
        Builder(Section.Builder parentBuilder, Consumer<ParagraphWithList> callback) throws Exception {
            super(parentBuilder,callback,ParagraphWithList.class);
        }

        ParagraphWithList.Builder withText(String text)
        {
            instance.text = text;
            return this;
        }

        UnorderedList.Builder withList() throws Exception {
            Consumer<UnorderedList> callback = (obj)-> this.instance.list=obj;
            return new UnorderedList.Builder(this,callback);
        }
    }
}
