package lab4.v2;

import java.io.ByteArrayOutputStream;

public class Program {
    public static void main(String[]args) throws Exception {
        Document cv = Document.builder()
                .withTitle("Jan Kowalski - CV")
                .withPhoto(new Picture("https://dsa.com"))
                .addSection("Wykształcenie")
                .addParagraph("2000-2005 Przedszkole im. Króleny Snieżki w...")
                .addParagraph("2006-2012 im Ronalda Regana w ...")
                .addParagraph("...")
                .end()
                .addSection("Umiejętności")
                .addParagraphWithList("Umiejętności")
                .withList()
                .addItem(new ListItem("C"))
                .addItem(new ListItem("C++"))
                .addItem(new ListItem("Java"))
                .end()
                .end()
                .end()
                .build();

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        cv.writeHTML(stream);
        String result = stream.toString();
        System.out.println(result);
    }
}
