package lab4.v2;

import lab4.framework.NestedBuilder;

import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class UnorderedList implements Element {
    private  List<ListItem> items = new ArrayList<>();

    private UnorderedList(){
    }

    @Override
    public void writeHTML(OutputStream stream) {
        PrintStream out = new PrintStream(stream);
        out.append("<ul>");
        items.forEach(x->x.writeHTML(stream));
        out.append("</ul>");
    }
    static class Builder extends NestedBuilder<ParagraphWithList.Builder,UnorderedList> {
        Builder(ParagraphWithList.Builder parentBuilder, Consumer<UnorderedList> callback) throws Exception {
            super(parentBuilder,callback,UnorderedList.class);
        }
        Builder addItem(ListItem item)
        {
            instance.items.add(item);
            return this;
        }
    }
}
