package lab4.v2;

import lab4.framework.NestedBuilder;

import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class Section implements Element {
    private  String title;
    private  List<Paragraph> paragraphs = new ArrayList<>();

    private Section(){
    }
    @Override
    public void writeHTML(OutputStream stream) {
        PrintStream out = new PrintStream(stream);
        out
            .append("<h2>")
            .append(this.title)
            .append("</h2>");

        paragraphs.forEach(x->x.writeHTML(out));
    }
    public static class Builder extends NestedBuilder<Document.Builder,Section> {
        Builder(Document.Builder builder, Consumer<Section> callback) throws Exception {
            super(builder,callback,Section.class);
        }
        Builder withTitle(String title)
        {
            instance.title = title;
            return this;
        }

        Builder addParagraph(String text)
        {
            this.instance.paragraphs.add(new Paragraph(text));
            return this;
        }

        ParagraphWithList.Builder addParagraphWithList(String text) throws Exception {
            Consumer<ParagraphWithList> onBuildFinished = (obj)->instance.paragraphs.add(obj);
            return new ParagraphWithList.Builder(this,onBuildFinished).withText(text);
        }
    }
}
