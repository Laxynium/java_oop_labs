package lab4.v2;

import lab4.framework.BuilderBase;

import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public final class Document implements Element{
    private String title;
    private Picture picture;
    private List<Section> sections = new ArrayList<>();
    private Document() {
    }

    public static Builder builder() throws Exception {
        return new Document.Builder();
    }

    @Override
    public void writeHTML(OutputStream stream) {
        PrintStream out = new PrintStream(stream);
        out.printf("<!DOCTYPE html>" +
                "<html>" +
                "<head>" +
                "<title>%s</title>" +
                "</head>",title);
        out.print("<body>");
        out.printf("<h1>%s</h1>",title);
        picture.writeHTML(out);
        sections.forEach(x->x.writeHTML(out));
        out.print("</body>");
        out.print("</html>");
    }

    static class Builder extends BuilderBase<Document> {
        Builder() throws Exception {
            super(Document.class);
        }

        Builder withTitle(String title){
            instance.title = title;
            return this;
        }

        Builder withPhoto(Picture picture){
            instance.picture = picture;
            return this;
        }

        Builder addSection(Section section)
        {
            instance.sections.add(section);
            return this;
        }

        Section.Builder addSection(String title) throws Exception {
            Consumer<Section> f = obj-> instance.sections.add(obj);
            return new Section.Builder(this,f).withTitle(title);
        }

        @Override
        protected boolean validate() {
            return instance.title != null && instance.picture != null && !instance.sections.isEmpty();
        }
    }

}
