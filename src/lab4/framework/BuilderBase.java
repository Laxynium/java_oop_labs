package lab4.framework;

import java.lang.reflect.Constructor;

public abstract class BuilderBase<T>{
    protected T instance;
    public BuilderBase(Class<T>clazz) throws Exception{
        Constructor<T> ctor = clazz.getDeclaredConstructor();
        ctor.setAccessible(true);
        instance = ctor.newInstance();
    }
    protected boolean validate(){
        return true;
    }
    public T build(){
        if(validate())
            return instance;
        return null;
    }

}
