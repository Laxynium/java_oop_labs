package lab4.framework;

import java.lang.reflect.Constructor;
import java.util.function.Consumer;

public abstract class NestedBuilder<Parent,T> {
    private Parent parent;
    private Consumer<T> callback;
    protected T instance;
    public NestedBuilder(Parent parent, Consumer<T>callback, Class<T> clazz) throws Exception {
        this.parent = parent;
        this.callback = callback;

        Constructor<T> ctor = clazz.getDeclaredConstructor();
        ctor.setAccessible(true);
        instance = ctor.newInstance();
    }

    public Parent end(){
        callback.accept(instance);
        return parent;
    }

}
