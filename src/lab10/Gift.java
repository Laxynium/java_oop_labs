package lab10;

import java.awt.*;

public class Gift implements XmasShape{
    private int y;
    private int x;
    private final int width;
    private final int height;
    private Color color;

    public Gift(int x, int y,int width,int height,Color color) {
        this.y = y;
        this.x = x;
        this.width = width;
        this.height = height;
        this.color = color;
    }

    @Override
    public void transform(Graphics2D g2d) {
        g2d.translate(this.x,this.y);
    }

    @Override
    public void render(Graphics2D g2d) {
        g2d.setColor(color);
        g2d.fillRect(x,y,width,height);
    }
}
